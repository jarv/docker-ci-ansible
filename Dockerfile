FROM python:3-alpine

COPY Pipfile /Pipfile
COPY Pipfile.lock /Pipfile.lock

LABEL version="0.0.1" \
      maintaner="Yakshaving.art" \
      description="Image we use on gitlab CI for running ansible stuff"

RUN apk add curl bash openssh-client openssl-dev libffi-dev alpine-sdk git \
    && pip install --no-cache-dir pipenv && pipenv lock -r > requirements.txt \
    && cat requirements.txt && pip install --no-cache-dir -r requirements.txt \
    && apk del libffi-dev alpine-sdk \
    && pip uninstall -y pipenv \
    && echo "done"

CMD ["/bin/bash"]
